## MRI Database of Multiple Sclerosis Patients with Consensus Ground Truth WM Lesion Segmentations


This archive contains Magnetic Resonance images of Multiple Sclerosis (MS) patients with corresponding consensus based ground truth segmentations of white matter lesions. This repository is a re-organization of [a recently published dataset](http://lit.fe.uni-lj.si/tools.php?lang=eng#3D%20MR%20image%20database%20of%20Multiple%20Sclerosis%20patients%20with%20white%20matter%20lesion%20segmentations) to meet the needs of the `dl4ni` package.

The original dataset, containing both raw and preprocessed images, is organized into patients' folders, as documented in [its website](http://lit.fe.uni-lj.si/showpdf.php?lang=eng&type=doc&doc=db-3d-ms-i). That same organization has been used in [John Muschelli](http://johnmuschelli.com/)'s [`open_ms_data` repo](https://github.com/muschellij2/open_ms_data), where I first found the dataset. Note that, in order to preserve patients' privacy **all images were defaced**. Please, see the *References* section below for more details.


### Folder structure ###


This dataset has been re-structured to be used by the `dl4ni` package. Now, all files have a prefix `xx_` indicating the patient ID in the original repo.

- `inputs`
    - `T1`
        - `01_T1W.nii.gz`, ..., `15_T1W.nii.gz` (T1 images, bias corrected, and coregistered to FLAIR modality)
    - `T2`
        - `01_T2W.nii.gz`, ..., `15_T2W.nii.gz` (T2 images, bias corrected, and coregistered to FLAIR modality)
    - `T1Gad`
        - `01_T1WKS.nii.gz`, ..., `15_T1WKS.nii.gz` (contrast-enhanced T1 images, bias corrected, and coregistered to FLAIR modality)
    - `FLAIR`
        - `01_FLAIR.nii.gz`, ..., `15_FLAIR.nii.gz` (FLAIR images, bias corrected)
- `outputs`
    - `brainmask`
        - `01_brainmask.nii.gz`, ..., `15_brainmask.nii.gz` (brain masks for each patient)
    - `lesions`
        - `01_consensus_gt.nii.gz`, ..., `15_consensus_gt.nii.gz` (Consensus white matter lesion segmentations)
        
### How to get this dataset ###

From inside an [R](http://cran.r-project.org) session or [RStudio](https://www.rstudio.com), load the [dl4ni](https://bitbucket.org/neuroimaginador/dl4ni) package (you have to install it first) and run the following:

```
#!R
library(dl4ni) # Loads the package

path <- get_dataset("ms_lesions") # Returns the path where the dataset has been downloaded to.

```

### License ###

The database is released under the Creative-Commons Attribution (CC-BY) license. Please cite the first reference below in any published work that uses this database.


### References ###

- Lesjak, Žiga, Franjo Pernuš, Boštjan Likar, and  Žiga Špiclin. "A Novel Public MR Image Dataset of Multiple Sclerosis Patients With Lesion Segmentations Based on Multi-rater Consensus", Neuroinformatics (2017), DOI: 10.1007/s12021-017-9348-7
- Original dataset in was published on: http://lit.fe.uni-lj.si/tools
